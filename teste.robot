***Settings***
Library         String
Library         FakerLibrary    locale=pt_BR
Library         SeleniumLibrary


***Keywords***

Open Chrome
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Call Method    ${chrome_options}    add_argument    --disable-dev-shm-usage
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size     1366    768

***Test Cases***
Utiliza biblioteca de string
    ${str}                          Replace String	            Hello, world!	world	tellus	
    Should Be Equal	                ${str}	                    Hello, tellus!	
    Log To Console                  ${str}

Utiliza biblioteca faker 
    ${return}                       Random Letter
    Log To Console                  ${return}

Utiliza Selenium
    Open Chrome
    Go to                           http://training-wheels-protocol.herokuapp.com/dropdown 
    Select From List By Value       id:dropdown                     6
    ${selected}=                    Get Selected List Label         id:dropdown                 
    Should Be Equal                 ${selected}                     Loki